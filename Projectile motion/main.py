import numpy as np
import matplotlib.pyplot as plt


###########################################
#                                         #
#   Functions to calculate next value     #
#                                         #
###########################################

def next_position(previous, v, dt):
    """
    Computes position x[i] or y[i]
    from x[i-1] or y[i-1] after change
    of time dt

    :param previous: previous position
    :param v: speed in this direction
    :param dt: change in time
    """
    return previous + v * dt
    
def next_vy(is_case_v, prev, dt, a, b, c, g, m, speed):
    if is_case_v:
        return next_vy_case_1(prev, dt, c, g)
    else:
        return next_vy2(prev, dt, speed, a, b, g, m)


def next_vx_case_1(prev, dt, c):
    """
    Computes the following x-axis
    speed in case ~v, e. i. v_0 < v_c

    :param prev: previous speed
    :param dt: change in time
    :param c: constant, h / mass
    """
    return prev * (1 - c * dt)


def next_vy_case_1(prev, dt, c, g):
    """
    Computes the next y-axis
    speed in case ~v

    :param prev: previous speed
    :param dt: change in time
    :param c: constant, h / mass
    :param g: gravitation acceleration
    """
    return prev * (1 - c * dt) - g * dt


def next_vx_case_2(prev, dt, speed, a, b, m):
    """
    Computes the following x-axis
    speed in case ~v^2, e. i. v_0 > v_c

    :param prev: previous speed
    :param dt: change in time
    :param speed: length of previous velocity vector
    :param a: constant, h / (2 * v_c)
    :param b: constant, h * v_c / 2
    :param m: object's mass
    """
    return prev - (a * prev * speed + b) / m * dt


def next_vy2(prev, dt, speed, a, b, g, m):
    """
    Computes the following y-axis
    speed in case ~v^2, e. i. v_0 > v_c

    :param prev: previous speed
    :param dt: change in time
    :param speed: length of previous velocity vector
    :param a: constant, h / (2 * v_c)
    :param b: constant, h * v_c / 2
    :param g: gravitation acceleration
    :param m: object's mass
    """
    return prev - g * dt - (a * prev * speed + b) / m * dt


###########################################
#                                         #
#   Function to do numeric integration   #
#                                         #
###########################################

def compute_positions(pos_x, pos_y, velocity_0, dt, g, a, b, c, m, re_bound, case_change):
    """
    Simulates numeric integration with positions and
    speed. Fills values in pos_x and pos_y arrays.

    :param pos_x: Array of x-positions in time
    :param pos_y: Array of y-positions in time
    :param velocity_0: 2D vector of speed_0
    :param dt: change in time to use
    :param g: gravitation acceleration
    :param a: constant, h / (2 * v_c)
    :param b: constant, h * v_c / 2
    :param c: constant, h / mass
    :param m: object's mass
    :param re_bound: determines which case (~v or ~v^2) to use
    :param case_change: array, filled in with information whether ~v or ~v^2

    Both pos_x and pos_y must contain exactly one element -
    - position in time t = 0
    """
    i = 0  # points to the end of pos_{x, y} array
    velocity_x = [velocity_0[0]]
    velocity_y = [velocity_0[1]]
    last_case = True  # true for ~v

    while pos_y[i] >= 0:
        speed = np.sqrt(velocity_x[i] ** 2 + velocity_y[i] ** 2)
        if speed < re_bound:
            if not last_case:
                last_case = True
                case_change.append(i)
            new_x = next_position(pos_x[i], velocity_x[i], dt)
            new_y = next_position(pos_y[i], velocity_y[i], dt)
            new_vx = next_vx_case_1(velocity_x[i], dt, c)
            new_vy = next_vy_case_1(velocity_y[i], dt, c, g)

        else:
            if last_case:
                last_case = False
                case_change.append(i)
            new_x = next_position(pos_x[i], velocity_x[i], dt)
            new_y = next_position(pos_y[i], velocity_y[i], dt)
            new_vx = next_vx_case_2(velocity_x[i], dt, speed, a, b, m)
            new_vy = next_vy2(velocity_y[i], dt, speed, a, b, g, m)

        pos_x.append(new_x)
        pos_y.append(new_y)

        velocity_x.append(new_vx)
        velocity_y.append(new_vy)

        i += 1


###########################################
#                                         #
#              Main function              #
#                                         #
###########################################

def projectile_motion(mass, density, diameter, viscosity, speed, angle):
    """
    Given parameters, plots graph of x and y positions of a ball
    thrown at given angle

    :param mass: mass of the ball [kg]
    :param density: density of the environment [kg m^-3]
    :param diameter: diameter of the ball [m] {2 * radius}
    :param viscosity: viscosity of the environment
    :param speed: speed of the throw [m s^-1] {length of the vector}
    :param angle: angle in radians
    """
    # CONSTANTS
    grav_acceleration = 9.81
    delta_time = 0.001

    # VECTORS, ARRAYS
    velocity_0 = np.array([speed * np.cos(angle), speed * np.sin(angle)])
    pos_x = [0]
    pos_y = [0]
    case = []

    # VALUES
    surface = 4 * np.pi * (diameter / 2) ** 2
    h = 3 / 16 * surface * density
    re_bound = (10 ** 3) * viscosity / (diameter * density)
    a = 1 / 2 * h / re_bound
    b = 1 / 2 * h * re_bound

    compute_positions(pos_x, pos_y, velocity_0, delta_time, grav_acceleration, a, b, h / mass, mass, re_bound, case)
    case.append(len(pos_x) - 1)

    # if angle is pi/2, pos_x is always 0
    if np.isclose([angle], [np.pi / 2]):
        pos_x = [0] * len(pos_x)

    # plt.plot(pos_x, pos_y, '.')

    prev = 0
    for i in range(len(case)):
        if i % 2 == 0:
            plt.plot(pos_x[prev:case[i]], pos_y[prev:case[i]], 'go-', label='Case ~v')
        else:
            plt.plot(pos_x[prev:case[i]], pos_y[prev:case[i]], 'ro-', label='Case ~v^2')
        prev = case[i]
    plt.xlabel('Position X (m)')
    plt.ylabel('Position Y (m)')
    plt.show()

    plt.ion()


if __name__ == "__main__":
    projectile_motion(1, 3000, 0.1, 0.2, 35, np.pi / 4)
